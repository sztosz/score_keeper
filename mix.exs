defmodule ScoreKeeper.MixProject do
  use Mix.Project

  def project do
    [
      app: :score_keeper,
      version: "0.1.0",
      elixir: "~> 1.16",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {ScoreKeeper.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:bandit, "~> 1.0"},
      {:ecto_sql, "~> 3.11"},
      {:ecto_ulid_next, "~> 1.0.2"},
      {:jason, "~> 1.2"},
      {:postgrex, ">= 0.0.0"},
      {:req, "~> 0.4.0"}
    ]
  end
end
