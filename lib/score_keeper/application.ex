defmodule ScoreKeeper.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false
  use Application

  @impl true
  def start(_type, _args) do
    children = [
      ScoreKeeper.Repo,
      ScoreKeeper.Score,
      {Bandit, plug: ScoreKeeper.Router, port: 4001}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ScoreKeeper.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
