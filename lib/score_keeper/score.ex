defmodule ScoreKeeper.Score do
  use GenServer

  alias ScoreKeeper.Repo
  alias ScoreKeeper.Person

  import Ecto.Query

  @url Application.compile_env(:score_keeper, :url)
  @max_size Application.compile_env(:score_keeper, :max_size)
  @rate_limit_per_minute Application.compile_env(:score_keeper, :rate_limit_per_minute)
  @minute_in_ms 1000 * 60

  def start_link(_) do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end

  ## Callbacks

  @impl true
  def init(_) do
    {:ok, %{}}
  end

  @impl true
  def handle_cast(:update_scores, state) do
    ulids_to_process =
      Person
      |> where([p], p.score_update_at < ^DateTime.utc_now())
      |> Repo.all()
      |> Enum.map(fn e -> e.ulid end)
      |> Enum.chunk_every(@max_size)

    GenServer.cast(__MODULE__, {:get_next_batch_and_process, ulids_to_process})

    {:noreply, state}
  end

  @impl true
  def handle_cast({:get_next_batch_and_process, ulids_to_process}, state) do
    {process_now, process_later} = Enum.split(ulids_to_process, @rate_limit_per_minute)

    Enum.map(process_now, fn inner_list ->
      Task.start_link(fn ->
        Req.new(
          url: @url,
          method: :post,
          headers: [{"Content-Type", "application/json"}],
          json: %{ulids: Enum.map(inner_list, fn ulid -> %{ulid: ulid} end)}
        )
        |> Req.Request.run_request()
      end)
    end)

    unless [] == process_later do
      Process.send_after(self(), {:get_next_batch_and_process, process_later}, @minute_in_ms)
    end

    {:noreply, state}
  end

  @impl true
  def handle_info({:get_next_batch_and_process, {ulids_to_process}}, state) do
    GenServer.cast(__MODULE__, {:get_next_batch_and_process, ulids_to_process})
    {:noreply, state}
  end

  # API

  def update_scores do
    GenServer.cast(__MODULE__, :update_scores)
  end
end
