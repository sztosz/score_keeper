defmodule ScoreKeeper.Person do
  use Ecto.Schema

  @primary_key {:ulid, Ecto.ULID, autogenerate: true}
  @foreign_key_type Ecto.ULID

  schema "people" do
    field(:score, :integer)
    field(:score_update_at, :utc_datetime_usec)
    timestamps()
  end
end
