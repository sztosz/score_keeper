defmodule ScoreKeeper.Router do
  use Plug.Router

  alias ScoreKeeper.Webhook

  plug(:match)

  plug(Plug.Logger)

  plug(Plug.Parsers,
    parsers: [:json],
    pass: ["application/json"],
    json_decoder: Jason
  )

  plug(:dispatch)

  get "/abc" do
    send_resp(conn, 200, "123")
  end

  post "/score_webhook" do
    case Webhook.process(conn.params()) do
      :ok -> send_resp(conn, 200, "OK")
      :error -> send_resp(conn, 500, "ERROR")
    end
  end

  match _ do
    send_resp(conn, 404, "oops")
  end
end
