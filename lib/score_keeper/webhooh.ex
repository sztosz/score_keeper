defmodule ScoreKeeper.Webhook do
  import Ecto.Query

  alias ScoreKeeper.Repo
  alias ScoreKeeper.Person

  def init(options) do
    # initialize options
    options
  end

  def process(%{"ulid" => ulid, "score" => score}) when is_integer(score) do
    person = Person |> where([p], p.ulid == ^ulid) |> Repo.one()

    with false <- is_nil(person),
         update_person <-
           Ecto.Changeset.change(person, %{score_update_at: DateTime.utc_now(), score: score}),
         {:ok, _} <- Repo.update(update_person) do
      :ok
    else
      _ -> :error
    end
  end

  def process(_) do
    :error
  end
end
