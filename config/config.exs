import Config

config :score_keeper, ScoreKeeper.Repo,
  database: "score_keeper_repo",
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  migration_primary_key: [name: :ulid, type: :binary_id],
  migration_foreign_key: [type: :binary_id],
  migration_timestamps: [type: :utc_datetime_usec]

config :score_keeper, ecto_repos: [ScoreKeeper.Repo]

config :score_keeper,
  url: "http://localhost:4000/score",
  rate_limit_per_minute: 100,
  max_size: 100
