defmodule ScoreKeeper.Repo.Migrations.CreatePeople do
  use Ecto.Migration

  def change do
    create table(:people, primary_key: false) do
      add :ulid, :binary_id, primary_key: true, null: false
      add :score, :integer, null: false
      add :score_update_at, :utc_datetime_usec, null: false
      timestamps()
    end
  end
end
