# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Responder.Repo.insert!(%Responder.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias ScoreKeeper.Person
alias ScoreKeeper.Repo


Enum.each(0..10_000, fn _ ->
  score_update_at = DateTime.utc_now() |> DateTime.add(-7, :day) |> DateTime.add(Enum.random(1..14), :day)

  Repo.insert!(
    %Person{score: Enum.random(1..1_000), score_update_at: score_update_at}
  )
end)
